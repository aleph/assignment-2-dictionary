%include "colon.inc"

section .data

colon "apple", key_apple
db "A fruit that grows on trees", 0

colon "computer", key_computer
db "An electronic device used for processing information", 0

colon "cat", key_cat
db "A small domesticated carnivorous mammal", 0

colon "book", key_book
db "A written or printed work consisting of pages glued or sewn together", 0

colon "mountain", key_mountain
db "A large natural elevation of the earth's surface", 0