import subprocess

test_inputs = ["apple", "computer", "cat", "book", "mountain", "", "asdasd", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"]
expected_outputs = ["A fruit that grows on trees", "An electronic device used for processing information", "A small domesticated carnivorous mammal", "A written or printed work consisting of pages glued or sewn together", "A large natural elevation of the earth's surface", "", "", ""]
expected_errors = ["", "", "", "", "", "Provided element is not found", "Provided element is not found", "Provided key is invalid"]
num_failures = 0

print("Starting tests")
print("-------------------")

for i in range(len(test_inputs)):
    process = subprocess.Popen(["./result"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate(input=test_inputs[i].encode())
    stdout = stdout.decode().strip()
    stderr = stderr.decode().strip()

    if stdout == expected_outputs[i] and stderr == expected_errors[i]:
        print("Test %s passed" % i)
    else:
        num_failures += 1
        print("Test %s failed" % i)
        print("Input: %s" % test_inputs[i])
        print("Awaited output: %s" % expected_outputs[i])
        print("Stdout: %s" % stdout.strip())
        print("Awaited error: %s" % expected_errors[i])
        print("Stderr: %s" % stderr.strip())
        print("Exit code: %s" % process.returncode)
    print("-------------------")

if num_failures == 0:
    print("All tests passed")
else:
    print("%d tests failed" % num_failures)